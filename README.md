# timing_root

Root repo for easy start stand

Requirements:
 - [docker](https://docs.docker.com/install/)
 - [docker-compose](https://docs.docker.com/compose/install/)

Commands:
  - `docker-compose pull` - pulling latest images
  - `docker-compose up -d` - starting containers in daemon mode
  - `docker-compose down` - stoping containers and removing network

use '-v' flag for down if you want clear database

You may backup data just by copy volume

Backend starts on http://localhost:8000

Frontend starts on http://localhost:8081

You may connect using you local ip adress from another computer or using global ip adress of machine